﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Helpers;
using System.Reflection;
using System.Windows;
using TumblrViewer.Business.Gateway;
using TumblrViewer.Business.Resources;
using TumblrViewer.Business.Services;
using TumblrViewer.Services;
using TumblrViewer.ViewModels;

namespace TumblrViewer
{
    public class Bootstrapper : PhoneBootstrapperBase
    {
        private PhoneContainer _container;

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnUnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                MessageBox.Show(messageBoxText: AppResources.MessageBodyUnhandledException);
                Application.Current.Terminate();
            });
        }

        protected override void Configure()
        {
            _container = new PhoneContainer();

            if (!Execute.InDesignMode)
                _container.RegisterPhoneServices(RootFrame);

            RegisterViewModels();
            RegisterServices();
        }

        private void RegisterServices()
        {
            _container.RegisterSingleton(service: typeof(INavService),
                                         key: typeof(INavService).Name,
                                         implementation: typeof(NavigationServiceWrapper));

            _container.RegisterSingleton(service: typeof(ISettingsService),
                                         key: typeof(ISettingsService).Name,
                                         implementation: typeof(WindowsPhoneSettingsService));

            _container.RegisterSingleton(service: typeof(IGateway),
                                         key: typeof(IGateway).Name,
                                         implementation: typeof(TumblrGateway));
        }

        private void RegisterViewModels()
        {
            foreach (var viewModelType in TypeHelper.GetDerivedTypesFor<BaseViewModel>())
            {
                _container.RegisterPerRequest(service: viewModelType,
                                              key: viewModelType.Name,
                                              implementation: viewModelType);
            }
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        // Ovveride so Caliburn can load view models from PCL library.
        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            return new[]
            {
                Assembly.GetExecutingAssembly(),
                typeof(BaseViewModel).Assembly
            };
        }
    }
}