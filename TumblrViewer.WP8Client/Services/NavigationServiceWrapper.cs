﻿using Caliburn.Micro;
using TumblrViewer.Business.Services;
using TumblrViewer.ViewModels;

namespace TumblrViewer.Services
{
    public class NavigationServiceWrapper : INavService
    {
        private readonly INavigationService _navigationService;

        public NavigationServiceWrapper(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void NavigateToViewModel<T>() where T : BaseViewModel
        {
            _navigationService.UriFor<T>().Navigate();
        }

        public INavUriBuilder<T> UriForViewModel<T>()
        {
            return new WindowsPhoneUriBuilder<T>(_navigationService.UriFor<T>());
        }
    }
}