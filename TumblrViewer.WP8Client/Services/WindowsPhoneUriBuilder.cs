﻿using Caliburn.Micro;
using System;
using System.Linq.Expressions;
using TumblrViewer.Business.Services;

namespace TumblrViewer.Services
{
    public class WindowsPhoneUriBuilder<T> : INavUriBuilder<T>
    {
        private readonly UriBuilder<T> _uriBuilder;

        public WindowsPhoneUriBuilder(UriBuilder<T> uriBuilder)
        {
            _uriBuilder = uriBuilder;
        }

        public void Navigate()
        {
            _uriBuilder.Navigate();
        }

        public INavUriBuilder<T> WithParam<TValue>(Expression<Func<T, TValue>> accessor, TValue val)
        {
            return new WindowsPhoneUriBuilder<T>(_uriBuilder.WithParam(accessor, val));
        }
    }
}