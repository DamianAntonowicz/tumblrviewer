﻿using System.IO.IsolatedStorage;
using TumblrViewer.Business.Services;

namespace TumblrViewer.Services
{
    public class WindowsPhoneSettingsService : ISettingsService
    {
        public void AddOrUpdate<T>(Setting<T> setting, object value)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(setting.Key))
                IsolatedStorageSettings.ApplicationSettings.Remove(setting.Key);

            IsolatedStorageSettings.ApplicationSettings.Add(setting.Key, value);
            IsolatedStorageSettings.ApplicationSettings.Save();
        }

        public void Remove<T>(Setting<T> setting)
        {
            IsolatedStorageSettings.ApplicationSettings.Remove(setting.Key);
        }

        public T GetValueOrDefault<T>(Setting<T> setting)
        {
            try
            {
                T value;
                if (IsolatedStorageSettings.ApplicationSettings.TryGetValue(setting.Key, out value))
                    return value;

                return GetDefaultValue(setting);
            }
            catch
            {
                return GetDefaultValue(setting);
            }
        }

        private T GetDefaultValue<T>(Setting<T> setting)
        {
            if (setting.DefaultValue != null)
                return setting.DefaultValue;

            return default(T);
        }
    }
}