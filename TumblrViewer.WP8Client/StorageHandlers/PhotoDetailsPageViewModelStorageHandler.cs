﻿using Caliburn.Micro;
using TumblrViewer.ViewModels;

namespace TumblrViewer.StorageHandlers
{
    public class PhotoDetailsPageViewModelStorage : StorageHandler<PhotoDetailsPageViewModel>
    {
        public override void Configure()
        {
            Property(x => x.PhotoUrl).InPhoneState()
                                     .RestoreAfterViewLoad();
        }
    }
}