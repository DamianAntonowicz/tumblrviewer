﻿using System.Windows.Controls;
using TumblrViewer.ViewModels;
using WP7Contrib.View.Transitions.Animation;

namespace TumblrViewer.Views
{
    public partial class LoginPage : AnimatedBasePage
    {
        private LoginPageViewModel ViewModel => DataContext as LoginPageViewModel;

        public LoginPage()
        {
            InitializeComponent();
            AnimationContext = LayoutRoot;

            Loaded += LoginPage_Loaded;
        }

        private void LoginPage_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Loaded -= LoginPage_Loaded;
            TextBoxUsername.Text = ViewModel.Username;
        }

        private void TextBoxUsername_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            // Update property manually because binding doesn't want to work.
            ViewModel.Username = TextBoxUsername.Text;
        }
    }
}