﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Controls;
using System.Windows.Navigation;
using TumblrViewer.Business.Model;
using TumblrViewer.Business.Resources;
using TumblrViewer.ViewModels;
using WP7Contrib.View.Transitions.Animation;

namespace TumblrViewer.Views
{
    public partial class UserPostsPage : AnimatedBasePage
    {
        private UserPostsPageViewModel ViewModel => DataContext as UserPostsPageViewModel;
        private readonly ProgressIndicator _progressIndicator = new ProgressIndicator();

        public UserPostsPage()
        {
            InitializeComponent();
            AnimationContext = LayoutRoot;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            SystemTray.SetProgressIndicator(this, _progressIndicator);

            ViewModel.Posts.CollectionChanged += Posts_CollectionChanged;
            ViewModel.PropertyChanged += ViewModel_PropertyChanged;
        }

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ViewModel.IsBusy))
            {
                // ProgressIndicator should be displayed only when there are some posts already.
                //
                if (ViewModel.Posts.Count > 0)
                {
                    if (ViewModel.IsBusy)
                    {
                        _progressIndicator.IsIndeterminate = true;
                        _progressIndicator.Text = AppResources.LabelLoadingMorePosts;
                        _progressIndicator.IsVisible = true;
                    }
                    else
                    {
                        _progressIndicator.IsVisible = false;
                    }
                }
            }
        }

        private void Posts_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (ViewModel.Posts.Count > 0)
            {
                // LoadingControl should be only displayed when list is empty.
                //
                ContentPanel.Children.Remove(LoadingControl);
            }
        }

        private void LongListSelectorPosts_OnItemRealized(object sender, ItemRealizationEventArgs e)
        {
            var post = e.Container.Content as Post;
            if (post == null)
                return;

            var offset = 2;
            var shouldLoadMorePosts = !ViewModel.IsBusy &&
                                       ViewModel.Posts.Count - ViewModel.Posts.IndexOf(post) <= offset;

            if (shouldLoadMorePosts)
            {
                ViewModel.LoadUserPostsAsync();
            }
        }

        private void LongListSelectorPosts_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Need call view model here because SelectedItem property is not bindable.
            //
            ViewModel.NavigateToPhotoDetailsPage(LongListSelectorPosts.SelectedItem as Post);
            LongListSelectorPosts.SelectedItem = null;
        }
    }
}