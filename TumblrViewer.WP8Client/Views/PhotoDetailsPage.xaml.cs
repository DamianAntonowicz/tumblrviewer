﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using TumblrViewer.Business.Resources;
using TumblrViewer.ViewModels;
using WP7Contrib.View.Transitions.Animation;

namespace TumblrViewer.Views
{
    public partial class PhotoDetailsPage : AnimatedBasePage
    {
        private PhotoDetailsPageViewModel ViewModel => DataContext as PhotoDetailsPageViewModel;

        public PhotoDetailsPage()
        {
            InitializeComponent();
            AnimationContext = LayoutRoot;
        }

        private void Image_OnImageFailed(object sender, ExceptionRoutedEventArgs e)
        {
            LoadingControl.IsLoading = false;
            LoadingControl.ErrorText = AppResources.MessageBodyNetworkError;
        }

        private void Image_OnImageOpened(object sender, RoutedEventArgs e)
        {
            LoadingControl.IsLoading = false;
        }

        private void LoadingControl_OnTryAgain(object sender, EventArgs e)
        {
            LoadingControl.ErrorText = "";
            LoadingControl.IsLoading = true;

            Image.Source = new BitmapImage(new Uri(ViewModel.PhotoUrl));
        }
    }
}