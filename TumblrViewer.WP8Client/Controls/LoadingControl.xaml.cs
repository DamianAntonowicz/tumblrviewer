﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using WP7Contrib.View.Controls.Extensions;

namespace TumblrViewer.Controls
{
    public partial class LoadingControl : UserControl
    {
        public bool UseOverlay { get; set; }

        public static readonly DependencyProperty TryAgainMessageProperty =
            DependencyProperty.Register("TryAgainMessage",
                                        typeof(string),
                                        typeof(LoadingControl),
                                        new PropertyMetadata(OnTryAgainMessageChanged));

        public string TryAgainMessage
        {
            get { return (string)GetValue(TryAgainMessageProperty); }
            set { SetValue(TryAgainMessageProperty, value); }
        }

        public static readonly DependencyProperty LoadingMessageProperty =
            DependencyProperty.Register("LoadingMessage",
                                        typeof(string),
                                        typeof(LoadingControl),
                                        new PropertyMetadata(OnLoadingMessageChanged));

        public string LoadingMessage
        {
            get { return (string)GetValue(LoadingMessageProperty); }
            set { SetValue(LoadingMessageProperty, value); }
        }

        public static readonly DependencyProperty IsLoadingProperty =
            DependencyProperty.Register("IsLoading",
                                        typeof(bool),
                                        typeof(LoadingControl),
                                        new PropertyMetadata(OnIsLoadingChanged));

        public bool IsLoading
        {
            get { return (bool)GetValue(IsLoadingProperty); }
            set { SetValue(IsLoadingProperty, value); }
        }

        public static readonly DependencyProperty ErrorTextProperty =
            DependencyProperty.Register("ErrorText",
                                        typeof(string),
                                        typeof(LoadingControl),
                                        new PropertyMetadata(OnErrorTextChanged));

        public string ErrorText
        {
            get { return (string)GetValue(ErrorTextProperty); }
            set { SetValue(ErrorTextProperty, value); }
        }

        public event EventHandler TryAgain;

        public LoadingControl()
        {
            InitializeComponent();
        }

        private static void OnErrorTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var loadingControl = d as LoadingControl;

            if (e.NewValue == null)
            {
                loadingControl.StackPanelError.Hide();
                loadingControl.TextBlockError.Text = string.Empty;
                return;
            }

            var errorText = e.NewValue.ToString();

            if (string.IsNullOrWhiteSpace(errorText))
            {
                loadingControl.StackPanelError.Hide();
                loadingControl.TextBlockError.Text = string.Empty;
            }
            else
            {
                loadingControl.IsLoading = false;
                loadingControl.StackPanelError.Show();
                loadingControl.StackPanelLoading.Hide();
                loadingControl.ProgressBar.IsIndeterminate = false;
                loadingControl.TextBlockError.Text = errorText;
            }
        }

        private static void OnIsLoadingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var loadingControl = d as LoadingControl;

            if (loadingControl.IsLoading)
            {
                loadingControl.StackPanelError.Hide();
                loadingControl.StackPanelLoading.Show();
                loadingControl.ProgressBar.IsIndeterminate = true;

                if (loadingControl.UseOverlay)
                {
                    loadingControl.LayoutRoot.Background = new SolidColorBrush(Colors.Black);
                    loadingControl.LayoutRoot.Opacity = 0.8;
                }
            }
            else
            {
                loadingControl.StackPanelLoading.Hide();
                loadingControl.ProgressBar.IsIndeterminate = false;

                if (loadingControl.UseOverlay)
                {
                    loadingControl.LayoutRoot.Background = null;
                    loadingControl.LayoutRoot.Opacity = 1;
                }
            }
        }

        private static void OnLoadingMessageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var loadingControl = d as LoadingControl;
            loadingControl.TextBlockMessage.Text = e.NewValue.ToString();
        }

        private static void OnTryAgainMessageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var loadingControl = d as LoadingControl;
            loadingControl.ButtonTryAgain.Content = e.NewValue;
        }

        private void ButtonTryAgain_OnClick(object sender, RoutedEventArgs e)
        {
            if (TryAgain != null)
                TryAgain(this, EventArgs.Empty);
        }
    }
}
