﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using TumblrViewer.Business.Model;

namespace TumblrViewer.Converters
{
    public class PostToImageUrlConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var post = value as Post;
            return post.PhotoUrl.FirstOrDefault(x => x.MaxWidth == "100").Text;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}