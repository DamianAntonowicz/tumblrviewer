﻿using System.Resources;

namespace TumblrViewer.Common.Helpers
{
    public class Translator
    {
        private readonly ResourceManager _resourceManager;

        public string this[string key] => GetStringFromResources(key);

        public Translator(ResourceManager resourceManager)
        {
            _resourceManager = resourceManager;
        }

        private string GetStringFromResources(string key)
        {
            var @string = _resourceManager.GetString(key);

            if (string.IsNullOrEmpty(@string))
                @string = key;

            @string = @string.Replace("\\n", "\n"); // Need replace because if not then new line won't display.

            return @string;
        }
    }
}