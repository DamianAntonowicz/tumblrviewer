﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace System.Helpers
{
    public static class TypeHelper
    {
        public static IEnumerable<Type> GetDerivedTypesFor<T>()
            where T : class
        {
            var type = typeof(T);

            return type.GetTypeInfo()
                       .Assembly
                       .ExportedTypes
                       .Where(exportedType =>
                       {
                           var typeInfo = exportedType.GetTypeInfo();

                           return typeInfo.IsSubclassOf(type) &&
                                 !typeInfo.IsAbstract &&
                                  typeInfo.IsClass;
                       });
        }
    }
}