﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using TumblrViewer.Business.Gateway;
using TumblrViewer.Business.Model;
using TumblrViewer.Business.Requests;
using TumblrViewer.Business.Resources;
using TumblrViewer.Business.Responses;
using TumblrViewer.Business.Services;

namespace TumblrViewer.ViewModels
{
    public class UserPostsPageViewModel : BaseViewModel
    {
        private readonly IGateway _gateway;
        private readonly INavService _navigationService;

        private int MaxNumberOfPostsPerPage = 20;
        private int? _currentPostsOffset;
        private int? _totalPosts;
        private bool _areThereMorePosts = true;

        public ObservableCollection<Post> Posts { get; }

        public UserPostsPageViewModel(IGateway gateway, INavService navigationService)
        {
            _gateway = gateway;
            _navigationService = navigationService;

            Posts = new ObservableCollection<Post>();
        }

        public async void LoadUserPostsAsync()
        {
            if (!_areThereMorePosts)
                return;

            ErrorText = "";
            IsBusy = true;

            var nextPostsOffset = 0;

            if (_currentPostsOffset.HasValue)
                nextPostsOffset = _currentPostsOffset.Value + MaxNumberOfPostsPerPage;

            var response = await _gateway.MakeRequestAsync<GetUserPostsResponse>(
                new GetUserPostsRequest
                {
                    StartOffset = nextPostsOffset,
                    NumberOfPostsPerPage = MaxNumberOfPostsPerPage,
                    // For purpose of this project we restrict posts only to photos.
                    PostType = PostType.Photo
                });

            if (response.IsOk)
            {
                Posts.AddRange(response.Result.Posts.PostList);
                _currentPostsOffset = response.Result.Posts.Start;
                _totalPosts = response.Result.Posts.Total;
                _areThereMorePosts = _currentPostsOffset + MaxNumberOfPostsPerPage < _totalPosts;
            }
            else
            {
                ErrorText = AppResources.MessageBodyNetworkError;
            }

            IsBusy = false;
        }

        public void NavigateToPhotoDetailsPage(Post post)
        {
            if (post == null)
                return;

            _navigationService
                .UriForViewModel<PhotoDetailsPageViewModel>()
                .WithParam(x => x.PhotoUrl, post.PhotoUrl.FirstOrDefault(y => y.MaxWidth == "500").Text)
                .Navigate();
        }

        protected override void OnViewLoaded(object view)
        {
            LoadUserPostsAsync();
            base.OnViewLoaded(view);
        }
    }
}