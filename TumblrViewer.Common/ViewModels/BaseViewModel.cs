﻿using Caliburn.Micro;
using System.Collections.Generic;
using System.Resources;
using System.Runtime.CompilerServices;
using TumblrViewer.Business.Resources;
using TumblrViewer.Common.Helpers;

namespace TumblrViewer.ViewModels
{
    public abstract class BaseViewModel : Screen
    {
        private static readonly Translator _translator = new Translator(new ResourceManager(typeof(AppResources)));

        public Translator Translator => _translator;

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        private string _errorText;
        public string ErrorText
        {
            get { return _errorText; }
            set { SetProperty(ref _errorText, value); }
        }

        protected void SetProperty<TValue>(ref TValue fieldValue, TValue newValue, [CallerMemberName]string propertyName = "")
        {
            if (EqualityComparer<TValue>.Default.Equals(fieldValue, newValue))
                return;

            fieldValue = newValue;
            NotifyOfPropertyChange(propertyName);
        }
    }
}