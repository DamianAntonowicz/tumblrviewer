﻿namespace TumblrViewer.ViewModels
{
    public class PhotoDetailsPageViewModel : BaseViewModel
    {
        public string PhotoUrl { get; set; }
    }
}