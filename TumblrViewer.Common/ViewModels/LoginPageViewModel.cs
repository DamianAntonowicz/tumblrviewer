﻿using TumblrViewer.Business.Services;

namespace TumblrViewer.ViewModels
{
    public class LoginPageViewModel : BaseViewModel
    {
        private readonly INavService _navigationService;
        private ISettingsService _settingsService;

        private string _username;
        public string Username
        {
            get { return _username; }
            set
            {
                SetProperty(ref _username, value);
                CanNavigateToUserPostsPage = !string.IsNullOrWhiteSpace(Username);
            }
        }

        private bool _canNavigateToUserPostsPage;
        public bool CanNavigateToUserPostsPage
        {
            get { return _canNavigateToUserPostsPage; }
            set { SetProperty(ref _canNavigateToUserPostsPage, value); }
        }

        public LoginPageViewModel(INavService navigationService, ISettingsService settingsService)
        {
            _navigationService = navigationService;
            _settingsService = settingsService;

            Username = _settingsService.GetValueOrDefault(SettingKeys.Username);
        }

        public void NavigateToUserPostsPage()
        {
            _settingsService.AddOrUpdate(SettingKeys.Username, Username);
            _navigationService.NavigateToViewModel<UserPostsPageViewModel>();
        }
    }
}