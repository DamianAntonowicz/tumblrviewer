﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Xml.Serialization;

namespace TumblrViewer.Business.Gateway
{
    public abstract class BaseResponse<T> : BaseResponse
    {
        public T Result
        {
            get
            {
                return (T)_result;
            }
            set
            {
                _result = value;
            }
        }

        public BaseResponse()
        {
            _resultType = typeof(T);
        }
    }

    public abstract class BaseResponse
    {
        protected object _result;
        protected Type _resultType;

        public bool IsOk { get; set; }

        public Exception Error { get; set; }

        public string ContentType { get; set; }

        public CookieCollection Cookies { get; set; }

        public object GetResult()
        {
            return _result;
        }

        public virtual void SetResult(HttpWebResponse httpResponse, Stream resultStream)
        {
            try
            {
                if (resultStream == null)
                    return;

                if (IsResponseContentJson(httpResponse))
                {
                    var serializer = new DataContractJsonSerializer(_resultType);
                    _result = serializer.ReadObject(resultStream);
                }
                else if (IsResponseContentXml(httpResponse))
                {
                    var serializer = new XmlSerializer(_resultType);
                    _result = serializer.Deserialize(resultStream);
                }
                else
                {
                    _result = GetRawResponse(resultStream);
                }

                IsOk = true;
            }
            catch (Exception exception)
            {
                IsOk = false;
                Error = exception;
            }
        }

        private bool IsResponseContentJson(HttpWebResponse httpResponse)
        {
            return (httpResponse != null &&
                    httpResponse.ContentType.Contains(HttpType.ApplicationJson)) ||
                    ContentType == HttpType.ApplicationJson;
        }

        private bool IsResponseContentXml(HttpWebResponse httpResponse)
        {
            return (httpResponse != null &&
                    httpResponse.ContentType.Contains(HttpType.TextXml)) ||
                    ContentType == HttpType.TextXml;
        }

        private string GetRawResponse(Stream stream)
        {
            return stream.ReadToEnd();
        }
    }
}
