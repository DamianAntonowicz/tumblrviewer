﻿namespace TumblrViewer.Business.Gateway
{
    public class HttpType
    {
        public const string ApplicationJson = "application/json";
        public const string TextXml = "text/xml";
    }
}