﻿

using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TumblrViewer.Business.Gateway
{
    public interface IGateway
    {
        Task<TResponse> MakeRequestAsync<TResponse>(BaseRequest request)
            where TResponse : BaseResponse;
    }

    public class HttpGateway : IGateway
    {
        public async Task<TResponse> MakeRequestAsync<TResponse>(BaseRequest request)
            where TResponse : BaseResponse
        {
            return await Task.Factory.StartNew(() =>
            {
                var response = CreateResponse<TResponse>(request);

                try
                {
                    BeforeRequest(ref request);

                    var url = request.GetUrl();

                    if (request.HttpRequest.IgnoreCache)
                        url = AddIgnoreCacheParameter(url);

                    var httpRequest = WebRequest.CreateHttp(url);

                    if (!string.IsNullOrEmpty(request.HttpRequest.AcceptType))
                        httpRequest.Accept = request.HttpRequest.AcceptType;

                    httpRequest.ContentType = request.HttpRequest.ContentType;
                    httpRequest.CookieContainer = new CookieContainer();

                    BeforeHttpRequest(httpRequest);

                    Debug.WriteLine("==================================");
                    Debug.WriteLine("Sending request to: {0},\nwith headers:\n{1},\nmethod: {2},\nbody: {3}",
                                    url,
                                    httpRequest.Headers.ToString().Replace("\r", ""),
                                    request.HttpRequest.Method,
                                    request.GetRequestBody());

                    if (request.HttpRequest.Method == HttpMethod.POST)
                    {
                        httpRequest.Method = request.HttpRequest.Method.ToString();
                        httpRequest.BeginGetRequestStream((state) =>
                        {
                            try
                            {
                                var requestBody = request.GetRequestBody();
                                var body = Encoding.UTF8.GetBytes(requestBody);
                                using (var requestStream = httpRequest.EndGetRequestStream(state))
                                {
                                    requestStream.Write(body, 0, body.Length);
                                }

                                SendRequest(httpRequest, response);
                            }
                            catch (Exception error)
                            {
                                response.IsOk = false;
                                response.Error = error;
                            }
                        },
                            null);
                    }
                    else
                    {
                        SendRequest(httpRequest, response);
                    }
                }
                catch (Exception error)
                {
                    response.IsOk = false;
                    response.Error = error;
                }

                return response;
            });
        }

        private void SendRequest<TResponse>(HttpWebRequest httpRequest,
                                            TResponse response)
            where TResponse : BaseResponse
        {
            var manualResetEvent = new ManualResetEvent(false);

            httpRequest.BeginGetResponse(state =>
            {
                try
                {
                    var httpResponse = httpRequest.EndGetResponse(state) as HttpWebResponse;
                    response.Cookies = httpResponse.Cookies;

                    using (var responseStream = httpResponse.GetResponseStream())
                    {
#if DEBUG
                        using (var temporaryStream = new MemoryStream())
                        {
                            responseStream.CopyTo(temporaryStream);
                            temporaryStream.Position = 0;

                            var body = temporaryStream.ReadToEnd();

                            // Limit displayed body response because some responses may be very long.
                            var bodyLengthToTake = 10000;

                            if (body.Length < bodyLengthToTake)
                                bodyLengthToTake = body.Length;

                            Debug.WriteLine("======");
                            Debug.WriteLine("Response status: " + httpResponse.StatusCode);
                            Debug.WriteLine("Response body: " + body.Substring(0, bodyLengthToTake));
                            Debug.WriteLine("==================================");

                            temporaryStream.Position = 0;
                            response.SetResult(httpResponse, temporaryStream);
                        }
#else
                        response.SetResult(httpResponse, responseStream);
#endif
                    }
                }
                catch (Exception exception)
                {
                    response.Error = exception;
                    response.IsOk = false;
                }
                finally
                {
                    manualResetEvent.Set();
                }
            },
            null);

            manualResetEvent.WaitOne();
        }

        private string AddIgnoreCacheParameter(string url)
        {
            // By default web requests are cached in .NET based on url.
            // So we can get the same data when we call request twice in a row.
            // Data on server side can change so we end up with old data.
            // We are adding random parameter so requests are not cached.

            if (url.Contains("?"))
                url += "&r=" + DateTime.Now.Ticks;
            else
                url += "?r=" + DateTime.Now.Ticks;

            return url;
        }

        protected TResponse CreateResponse<TResponse>(BaseRequest request)
            where TResponse : BaseResponse
        {
            return Activator.CreateInstance(typeof(TResponse)) as TResponse;
        }

        protected virtual void BeforeHttpRequest(HttpWebRequest request)
        {
        }

        protected virtual void BeforeRequest(ref BaseRequest request)
        {
        }
    }
}
