﻿using TumblrViewer.Business.Requests;
using TumblrViewer.Business.Services;

namespace TumblrViewer.Business.Gateway
{
    public class TumblrGateway : HttpGateway
    {
        private readonly ISettingsService _settingsService;

        public TumblrGateway(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        protected override void BeforeRequest(ref BaseRequest request)
        {
            var baseTumblrRequest = request as BaseTumblrRequest;
            if (baseTumblrRequest == null)
                return;

            baseTumblrRequest.Username = _settingsService.GetValueOrDefault(SettingKeys.Username);
        }
    }
}