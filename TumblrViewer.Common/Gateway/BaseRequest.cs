﻿using System;

namespace TumblrViewer.Business.Gateway
{
    public abstract class BaseRequest
    {
        public HttpRequest HttpRequest { get; set; }

        protected BaseRequest()
        {
            HttpRequest = new HttpRequest();
            HttpRequest.Method = HttpMethod.GET;
        }

        public virtual string GetUrl()
        {
            throw new NotImplementedException();
        }

        public virtual string GetRequestBody()
        {
            return string.Empty;
        }
    }

    public class HttpRequest
    {
        public HttpMethod Method { get; set; }
        public string AcceptType { get; set; }
        public string ContentType { get; set; }
        public bool IgnoreCache { get; set; }
    }
}