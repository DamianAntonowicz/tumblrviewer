﻿namespace TumblrViewer.Business.Requests
{
    public class GetUserPostsRequest : BaseTumblrRequest
    {
        private const string StartOffsetParameterName = "start";
        private const string NumberOfPostsPerPageParameterName = "num";
        private const string PostTypeParameterName = "type";

        public int StartOffset { get; set; }
        public int NumberOfPostsPerPage { get; set; }
        public string PostType { get; set; }

        public override string GetUrl()
        {
            return base.GetUrl() + $"?{StartOffsetParameterName}={StartOffset}" +
                                   $"&{NumberOfPostsPerPageParameterName}={NumberOfPostsPerPage}" +
                                   $"&{PostTypeParameterName}={PostType}";
        }
    }
}