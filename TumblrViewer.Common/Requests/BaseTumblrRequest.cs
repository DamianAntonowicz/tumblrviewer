﻿using TumblrViewer.Business.Gateway;

namespace TumblrViewer.Business.Requests
{
    public abstract class BaseTumblrRequest : BaseRequest
    {
        public string Username { get; set; }

        protected BaseTumblrRequest()
        {
            HttpRequest.IgnoreCache = true;
        }

        public override string GetUrl()
        {
            return $"http://{Username}.tumblr.com/api/read/";
        }
    }
}