﻿using System.Xml.Serialization;

namespace TumblrViewer.Business.Model
{
    [XmlRoot(ElementName = "tumblr")]
    public class Tumblr
    {
        [XmlElement(ElementName = "tumblelog")]
        public Tumblelog Tumblelog { get; set; }

        [XmlElement(ElementName = "posts")]
        public Posts Posts { get; set; }

        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
    }
}