using System.Collections.Generic;
using System.Xml.Serialization;

namespace TumblrViewer.Business.Model
{
    [XmlRoot(ElementName = "photoset")]
    public class Photoset
    {
        [XmlElement(ElementName = "photo")]
        public List<Photo> Photos { get; set; }
    }
}