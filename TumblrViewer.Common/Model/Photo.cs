using System.Collections.Generic;
using System.Xml.Serialization;

namespace TumblrViewer.Business.Model
{
    [XmlRoot(ElementName = "photo")]
    public class Photo
    {
        [XmlElement(ElementName = "photo-url")]
        public List<Photourl> PhotoUrl { get; set; }

        [XmlAttribute(AttributeName = "offset")]
        public string Offset { get; set; }

        [XmlAttribute(AttributeName = "caption")]
        public string Caption { get; set; }

        [XmlAttribute(AttributeName = "width")]
        public string Width { get; set; }

        [XmlAttribute(AttributeName = "height")]
        public string Height { get; set; }
    }
}