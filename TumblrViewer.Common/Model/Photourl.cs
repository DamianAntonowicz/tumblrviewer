using System.Xml.Serialization;

namespace TumblrViewer.Business.Model
{
    [XmlRoot(ElementName = "photo-url")]
    public class Photourl
    {
        [XmlAttribute(AttributeName = "max-width")]
        public string MaxWidth { get; set; }

        [XmlText]
        public string Text { get; set; }
    }
}