using System.Collections.Generic;
using System.Xml.Serialization;

namespace TumblrViewer.Business.Model
{
    [XmlRoot(ElementName = "post")]
    public class Post
    {
        [XmlElement(ElementName = "photo-caption")]
        public string PhotoCaption { get; set; }

        [XmlElement(ElementName = "photo-url")]
        public List<Photourl> PhotoUrl { get; set; }

        [XmlElement(ElementName = "photoset")]
        public Photoset Photoset { get; set; }

        [XmlElement(ElementName = "tag")]
        public List<string> Tag { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "url")]
        public string Url { get; set; }

        [XmlAttribute(AttributeName = "url-with-slug")]
        public string UrlWithSlug { get; set; }

        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlAttribute(AttributeName = "date-gmt")]
        public string DateGmt { get; set; }

        [XmlAttribute(AttributeName = "date")]
        public string Date { get; set; }

        [XmlAttribute(AttributeName = "unix-timestamp")]
        public string UnixTimestamp { get; set; }

        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set; }

        [XmlAttribute(AttributeName = "reblog-key")]
        public string ReblogKey { get; set; }

        [XmlAttribute(AttributeName = "slug")]
        public string Slug { get; set; }

        [XmlAttribute(AttributeName = "width")]
        public string Width { get; set; }

        [XmlAttribute(AttributeName = "height")]
        public string Height { get; set; }

        [XmlElement(ElementName = "question")]
        public string Question { get; set; }

        [XmlElement(ElementName = "answer")]
        public string Answer { get; set; }

        [XmlElement(ElementName = "photo-link-url")]
        public string PhotoLinkUrl { get; set; }
    }
}