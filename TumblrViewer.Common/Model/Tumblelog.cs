using System.Xml.Serialization;

namespace TumblrViewer.Business.Model
{
    [XmlRoot(ElementName = "tumblelog")]
    public class Tumblelog
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "timezone")]
        public string Timezone { get; set; }
    }
}