using System.Collections.Generic;
using System.Xml.Serialization;

namespace TumblrViewer.Business.Model
{
    [XmlRoot(ElementName = "posts")]
    public class Posts
    {
        [XmlElement(ElementName = "post")]
        public List<Post> PostList { get; set; }

        [XmlAttribute(AttributeName = "start")]
        public int Start { get; set; }

        [XmlAttribute(AttributeName = "total")]
        public int Total { get; set; }
    }
}