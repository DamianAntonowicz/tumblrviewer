﻿namespace TumblrViewer.Business.Model
{
    public class PostType
    {
        public const string Text = "text";
        public const string Quote = "quote";
        public const string Photo = "photo";
        public const string Link = "link";
        public const string Chat = "chat";
        public const string Video = "video";
        public const string Audio = "audio";
    }
}