﻿namespace System.Collections
{
    public static class IListExtensions
    {
        public static void AddRange(this IList collection, IEnumerable items)
        {
            foreach (var item in items)
            {
                collection.Add(item);
            }
        }
    }
}