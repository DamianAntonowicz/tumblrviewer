﻿namespace System.IO
{
    public static class StreamExtensions
    {
        public static string ReadToEnd(this Stream stream)
        {
            var streamReader = new StreamReader(stream);
            return streamReader.ReadToEnd();
        }
    }
}