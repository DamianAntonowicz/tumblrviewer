﻿using System;
using System.Linq.Expressions;

namespace TumblrViewer.Business.Services
{
    public interface INavUriBuilder<T>
    {
        void Navigate();
        INavUriBuilder<T> WithParam<TValue>(Expression<Func<T, TValue>> accessor, TValue val);
    }
}