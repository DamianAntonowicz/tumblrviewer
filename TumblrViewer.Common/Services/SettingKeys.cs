﻿namespace TumblrViewer.Business.Services
{
    public static class SettingKeys
    {
        public static Setting<string> Username = new Setting<string>
        {
            Key = nameof(Username),
            DefaultValue = ""
        };
    }
}