﻿using TumblrViewer.ViewModels;

namespace TumblrViewer.Business.Services
{
    public interface INavService
    {
        void NavigateToViewModel<T>()
            where T : BaseViewModel;

        INavUriBuilder<T> UriForViewModel<T>();
    }
}