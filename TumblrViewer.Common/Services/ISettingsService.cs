﻿namespace TumblrViewer.Business.Services
{
    public class Setting<T>
    {
        public string Key { get; set; }
        public T DefaultValue { get; set; }
    }

    public interface ISettingsService
    {
        void AddOrUpdate<T>(Setting<T> setting, object value);
        void Remove<T>(Setting<T> setting);
        T GetValueOrDefault<T>(Setting<T> setting);
    }
}